$(function() {
  $("#title").append('<h1 style="display: none" align="center" class="nocommunity">您尚未加入社區</h3>');
  $("#title").append('<h1 style="display: none" align="center" class="nomanager">您的社區管理員尚未加入使用平台</h1>');
  $("#title").append('<h1 style="display: none" align="center" class="nofeature">您的社區未開啟這項服務</h1>');
  
  $("#nocommunity").after("<div><p class='nocommunity_sentence'/></div>")
  var nocommunity_sentence_html = "這個功能需要加入社區才看得到社區的資訊。<br>"+
      "側邊欄 -> 住戶資訊 -> 社區申請紀錄 -> 加入社區 -> 選擇社區 -> 填寫註冊單 -> "+
      "送出社區申請單後請耐心等候 -> 可請鄰居幫你審核加入 或 社區有開啟物業服務 則你的管理員可以協助審核你<br>"+
      "ps. 如果你想加入的地址已經有其他家人加入,則只有你的家人可以審核你";
  $(".nocommunity_sentence").html(nocommunity_sentence_html);

  $("#nomanager").after("<div><p class='nomanager_sentence'/></div>")
  var nomanager_sentence_html = "邀請你的社區管理員/主委加入有無社區，提供這項服務。"+
      "你可以由下方的按鈕進入申請物業服務頁面，提供社區主委聯絡方式，我們將盡快與主委聯繫並討論後續導入事宜。";
  $(".nomanager_sentence").html(nomanager_sentence_html);

  $("#nofeature").after("<div><p class='nofeature_sentence'/></div>")
  var nofeature_sentence_html = "你的社區有可能有什麼原因，管理員/主委決定將功能關閉，你可以直接去詢問管理員/主委。";
  $(".nofeature_sentence").html(nofeature_sentence_html);

  $( "#accordion" ).accordion();
    var strUrl = location.search+"";
    if(strUrl==""){
      console.log("empty");
    } else{
      strUrl = strUrl.replace("?","");
      strUrl = strUrl.replace("status=","");
      $("#"+strUrl).click();
      $("."+strUrl).show();
      console.log(strUrl);
    }
    
  });